<?php

class GeonamesTaxonomy extends WebTaxonomy {

  /**
   * Implements WebTaxonomy::autocomplete().
   */
  public function autocomplete($string = '') {
    $matches = array();
    $web_tids = array();
    $server = variable_get('geonames_server', GEONAMES_SERVER_URL);
    $username = variable_get('geonames_username', '');
    $url = $server ."/searchJSON";
    $params = array(
      'name_startsWith' => $string,
      'featureClass' => 'P',
      'maxRows' => '20',
      'style' => 'medium',
    );
    return $this->fetchTermInfo($url, $params);
  }

  /**
   * TODO Implements WebTaxonomy::fetchTerm().
   *
   */
  public function fetchTerm($term) {

    $term_info = array();
    $web_tid = $term->web_tid[LANGUAGE_NONE][0]['value'];
    $term_name = $term->name;
    //drupal_set_message(t("term_name: %term_name",  array('%term_name' => $term_name)));
    if (!empty($web_tid)) {
      $params = array();
      $term_info = $this->fetchTermInfoXML($web_tid, $term_name, $params);
    }
    else {
      // @todo If there is no Web Tid, check by name.
      drupal_set_message(t("No web_tid found."));
    }

    return array_shift($term_info);
  }

  /*
   * fetch term info from geonames given a url and params
   *
   */
  protected function fetchTermInfo($url, $params) {
    $term_info = array();

    $http = http_client();
    $data = $http->get($url, $params);

    $result = json_decode($data);
    $geonames = $result->geonames;

    foreach ($geonames as $geoname) {
      $name = $geoname->name;
      $adminName = $geoname->adminName1;
      $countryName = $geoname->countryName;
      $geonameid = $geoname->geonameId;
      $term = $name .", ". $adminName .", ". $countryName;

      $term_info[$term] = array(
        'name' => $term,
        'web_tid' => 'http://sws.geonames.org/'. $geoname->geonameId ."/" ,
      );
    }

    return $term_info;

  }

  /*
   * Get linked data from geonames using the web_tid
   */
  protected function fetchTermInfoXML($web_tid, $term, $params) {

    $term_info = array();
    $client = http_client();
    $url = $web_tid ."about.rdf";
    $data = $client->get($url, $params);

    $xml = simplexml_load_string($data);
    if (version_compare(PHP_VERSION, '5.2.0', '>=')) {
      //Fetch all namespaces
      $namespaces = $xml->getNamespaces(true);
      //Register them with their prefixes
      foreach ($namespaces as $prefix => $ns) {
        $xml->registerXPathNamespace($prefix, $ns);
      }
    }

    $term_info[$term] = array(
      'name' => $term,
      'web_tid' => $web_tid,
    );

    return $term_info;
  }
}

