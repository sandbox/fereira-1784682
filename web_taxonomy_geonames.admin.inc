<?php

/**
 * @file
 * Administration-related functions for Web_Taxonomy_GeoNames
 */

/**
 * Admin Settings Page
 */
function web_taxonomy_geonames_admin_settings() {
  $form['geonames_server'] = array(
    '#type' => 'textfield',
    '#title' => t('URL to Geonames server to use.'),
    '#description' => t('Example: http://ws.geonames.org  - without the trailing slash.  Visit the !link page for more information', array('!link' => l(t('GeoNames Premium Webservices'), 'http://www.geonames.org/professional-webservices.html'))),
    '#default_value' => variable_get('geonames_server', GEONAMES_SERVER_URL),
  );
  $form['geonames_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('This is *required* for commercial and free services. You can !register.', array('!register' => l('register a GeoNames account', 'http://www.geonames.org/login'))),
    '#default_value' => variable_get('geonames_username', ''),
  );

  return system_settings_form($form);
}

/**
 * Admin Settings Page : Validation
 */
function web_taxonomy_geonames_admin_settings_validate($form, &$form_state) {
  // validate that the given settings work
  $url = $form_state['values']['geonames_server'] .'/search?username='. $form_state['values']['geonames_username'] .'&name=nydalen';
  $data = drupal_http_request($url);
  if ($data->code != '200') {
    form_set_error('', t('There is a problem with the response from the URL you have specified.') .'<br />'. t('The server returned errorcode %code: %error', array('%code' => $data->code, '%error' => $data->error)));
  }
  else {
    $xml = new SimpleXMLElement($data->data);
    if ($xml->status['message']) {
      form_set_error('', t('GeoNames Service Response: %message (code: %code)', array('%message' => $xml->status['message'], '%code' => $xml->status['value'])));
    }
    else {
      if ($xml->geoname[0]->name == 'Nydalen') {
        drupal_set_message(t('Your account has been successfully tested and is properly configured!'));
      }
      else {
        form_set_error('', t('Unknown Error'));
      }
    }
  }
}


